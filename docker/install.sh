#!/bin/sh
if [ "$(uname -s)" = "Darwin" ]; then
  # NOTE: Let's switch to Homebrew install here
  brew install docker docker-compose && \
    brew cask install kitematic
fi
